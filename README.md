# API-lab

An API in under 30 lines of code

### ARCHIVED repo

This repo is no longer maintained. 


## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://codeberg.org/miccaman/API-lab/tags).

## Authors

Miccaman

## License

[GPLv3](LICENCE)

## Acknowledgments

* Hat tip to anyone whose code was used
