# Changelog

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [Released]

## [0.4.0] - 2024-03-28
### Changed
- Close API lab issue tracking #2
- Archive REPO

## [0.3.0] - 2022-02-17
### Changed
- API lab issue tracking #2

## [0.2.2] - 2022-02-15
### Changed
- Make a codeberg PR

## [0.2.1] - 2022-01-19
### Changed
- Make the server listen to own PORT

## [0.2.0] - 2022-01-18
### Added
- src/index

## [0.1.2] - 2022-01-17
### Added
- NPM init (package.json) 
- Install express

## [0.1.1] - 2022-01-14
### Added
- This CHANGELOG file
